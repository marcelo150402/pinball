#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

using namespace std;

int main(int argc, char **argv) {

 SDL_Init(SDL_INIT_VIDEO);//Inicializa a SDL

 SDL_Window *janela = NULL;//Variavel da janela
 SDL_Surface *imagem = NULL;//Vari�vel da Imagem
 SDL_Surface *superficie = NULL;

 superficie = SDL_GetWindowSurface(janela);

 imagem = IMG_Load("imagemodelo.png");//carrega a imagem

 SDL_BlitSurface(imagem, NULL ,superficie ,NULL);//Desenha a imagem na janela
 SDL_UpdateWindowSurface(janela);//Atualiza a janela

 SDL_Delay(3000);//Espera 3 segundos

 SDL_FreeSurface(imagem);
 SDL_FreeSurface(superficie);//libera a imagem
 SDL_DestroyWindow(janela);
 SDL_Quit();

 return 0;
}
